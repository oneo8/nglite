The objective of this project is to create a light weight app with: 
	
	* newer technologies
	* new look & feel
	* responsive and mobile friendly website
	* modular development

### Technologies Used###
	* *Angular 2*
	* Typescript
	* SystemJS
	* Bootstrap
	* jquery
	* rxjs
